class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.date :date
      t.integer :user_id
      t.integer :room_id
      t.string :answer

      t.timestamps
    end
  end
end
