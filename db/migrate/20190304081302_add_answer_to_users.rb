class AddAnswerToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :answer, :string
  end
end
