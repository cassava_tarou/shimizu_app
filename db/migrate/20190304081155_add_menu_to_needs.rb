class AddMenuToNeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :needs, :menu, :string
  end
end
