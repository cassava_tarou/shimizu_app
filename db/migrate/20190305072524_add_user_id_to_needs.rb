class AddUserIdToNeeds < ActiveRecord::Migration[5.2]
  def change
    add_column :needs, :user_id, :integer
  end
end
