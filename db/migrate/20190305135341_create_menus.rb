class CreateMenus < ActiveRecord::Migration[5.2]
  def change
    create_table :menus do |t|
      t.date :date
      t.text :menu
      t.integer :room_id

      t.timestamps
    end
  end
end
