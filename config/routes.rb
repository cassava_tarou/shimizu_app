Rails.application.routes.draw do

  get 'post/index'
  get 'room/index'
  get "room/new" => "room#new"
  post "room/create" => "room#create"
  get "room/:id/show" => "room#show"
  post "room/:id/edit" => "room#edit"
  post "room/:id/:id/exit" => "room#destroy" 
  post "room/:id/create" => "room#create_needs"
  post "room/:id/:id/answer/edit" => "room#edit_answer"
  post "room/:id/:id/answer" => "room#create_answer"
  get "room/login_form" => "room#login_form"
  post "room/login" => "room#login"
  
  get "users/new" => "users#new"
  post "users/create" => "users#create"
  get "users/:id/show" => "users#show"
  get "users/login_form" => "users#login_form"
  post "users/login" => "users#login"
  post "users/:id/logout" => "users#logout"
  post "users/:id/edit" => "users#edit"
  
  get "/" => "home#top"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
