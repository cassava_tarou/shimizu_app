class UsersController < ApplicationController
  before_action :set_users ,{only:[:show,:login,:edit,]}
  before_action :already, {only:[:new,:login_form]}
  
  def set_users
      @user = User.find_by(id: session[:user_id])
  end
  
  def new
  end
  
  def create
    @user = User.new(name: params[:name],password: params[:password])
    if @user.save 
      flash[:notice] = "ユーザーを登録しました"
      session[:user_id] = @user.id
      redirect_to("/users/#{@user.id}/show")
    else
      flash[:notice] = "有効な名前、パスワードを入力してください"
    end
  end
  
  def show
    @user = User.find_by(id: params[:id])
  end
  
  def login_form
  end
  
  def login
    @user = User.find_by(name: params[:name],password: params[:password])
    if @user
      session[:user_id] = @user.id
      flash[:notice] = "ログインしました"
      if @user.room_id
        session[:room_id] = @user.room_id
        redirect_to("/room/#{@user.room_id}/show")
      else
        redirect_to("/users/#{@user.id}/show")
      end  
    else
      flash[:notice] = "ユーザー名またはパスワードが違います"
      render("/users/login_form")
    end
  end
  
  def edit
    @user.name = params[:name]
    @user.password = params[:password]
    if @user.save
      flash[:notice] = "編集しました"
      redirect_to("/users/#{@user.id}/show")
    else
      flash[:notice] = "有効なユーザー名パスワードを入力してください"
    end  
  end
  
  def logout
    session[:user_id] = nil
    redirect_to("/")
  end
end
