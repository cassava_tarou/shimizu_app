class ApplicationController < ActionController::Base
  
    def set_current_users
      @current_user = User.find_by(id: session[:user_id])
    end
    
    def authentic_user
      if session[:user_id] == nil
        flash[:notice] = "ログインが必要です"
        redirect_to("/users/login_form")
      end 
    end
    
    def currect_user
      if session[:room_id] != params[:room_id].to_i
        flash[:notice] = "権限がありません"
      　redirect_to("/")
      end
    end  
    
    def already
      if session[:user_id]
        flash[:notice] = "すでにログインしています"
        redirect_to("/")
      end
    end
    
end
