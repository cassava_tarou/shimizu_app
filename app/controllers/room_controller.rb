class RoomController < ApplicationController
  require "date"
  before_action :set_current_users,{only:[:create,:show,:login,:destroy]}
  before_action :set_current_rooms,{only:[:show,:edit,:create_needs,:create_answer,:edit_answer]}
  before_action :authentic_user,{only:[:login_form,:new]}
  
  def set_current_rooms
    @current_room = Room.find_by(id: session[:room_id])
  end
  
  def currect_user
      if session[:room_id] != @room.id.to_i
        flash[:notice] = "権限がありません"
      　redirect_to("/")
      end
  end 
  
  def index
    
  end
  def new
  end
  
  def create
    @room = Room.new(room_name: params[:name],password: params[:password])
    if @room.save
      flash[:notice] = "部屋が作成されました"
      session[:room_id] = @room.id
      redirect_to("/room/#{@room.id}/show")
      @user = User.find_by(id: session[:user_id])
      @user.room_id = @room.id
      @user.save
    else
      flash[:notice] = "有効な部屋の名前、パスワード入力してください"
      render("/room/new")
    end
  end
  

  

  
  def login_form
    
  end
  
  def login
    @room = Room.find_by(room_name: params[:room_name],password: params[:password])
    @user = User.find_by(id: session[:user_id])
    if @room
      @user.room_id = @room.id
      @user.save
      flash[:notice] = "部屋に入室しました"
      session[:room_id] = @room.id
      redirect_to("/room/:id/show")
    else
      flash[:notice] = "部屋の名前もしくはパスワードが違います"
      render("room/login_form")
    end
  end
  
  def show
    require "date"
    @menu = Menu.find_by(date: Date.today, room_id: session[:room_id])
    @answer = Answer.find_by(date: Date.today, user_id: session[:user_id])
  end
  
  def edit
    @current_room.room_name = params[:room_name]
    @current_room.password = params[:password]
    @currect_user.save
    redirect_to("/room/#{session[:room_id]}/show")
  end
  
  def destroy
    @current_user.room_id = nil
    @current_user.save
    session[:room_id] = nil
    redirect_to("/")
  end
    
    
  
  def create_needs
    @menu = Menu.new(date: Date.today,menu: params[:menu],room_id: session[:room_id])
    @menu.save
    redirect_to("/room/#{@current_room.id}/show")
    @answer = Answer.find_by(id: params[:id])
  end
  
  def create_answer
    @answer = Answer.new(date: Date.today, user_id: session[:user_id], 
                         answer: params[:answer],room_id: session[:room_id],
                         menu: Menu.find_by(date: Date.today))
    @answer.save
    redirect_to("/room/#{@current_room.id}/show")
  end
  
  def edit_answer
    @answer = Answer.find_by(user_id: session[:user_id], date: Date.today)
    @answer.answer = params[:edit_answer]
    @answer.save
    redirect_to("/room/#{@current_room.id}/show")
  end
  
end
