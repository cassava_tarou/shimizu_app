class Room < ApplicationRecord
    validates :room_name,{presence: true}
    validates :password ,{presence: true}
    has_many :menus
    has_many :users
  
end
